<?php
$footer_logo = get_field('footer_logo', 'option');
$footer_buttons = get_field('footer_buttons_top_bar_buttons', 'option');
$footer_content = get_field('footer_content', 'option');
$social_media = get_field('social_media', 'option');

?>
<div class="footer">
	<div class="footer__wrapper">
		<div class="footer__logo">
			<?php if($footer_logo) : ?>
				<a href="<?php bloginfo('url'); ?>"><img src="<?= $footer_logo['url']; ?>" alt=""></a>
			<?php endif; ?>
		</div>
		<div class="footer__section">
					<div class="footer__buttons">
						<?php
						if($footer_buttons){
			  			foreach($footer_buttons as $top_bar_button):
			  				$buttondata = $top_bar_button['nav_button_link'];
			  				$typebutton = ($top_bar_button['nav_button_type'] == "hollow") ? "primary" : "secondary" ;
			  			?>
							<a href="<?= $buttondata['url']; ?>" class="<?= $top_bar_button['nav_button_type']; ?> <?= $typebutton; ?> button " target="<?= $buttondata['target']; ?>"><?= $buttondata['title']; ?></a>

			  		<?php
			  			endforeach;
			  		}
			  		?>
					</div>
					<div class="footer__nav">
						<?php	if($social_media): foreach($social_media as $social):
							$social_link = $social['social_link'];
							$social_image = $social['social_image']; ?>
								<li><a href="<?= $social_link; ?>" target="_blank"><img src="<?= $social_image['url']; ?>" alt=""></a></li>

						<?php	endforeach; endif; ?>
			    	<?php
			  	    $default = array(
			  	      'theme_location'  => 'footer_menu',
			  	      'menu'            => '',
			  	      'container'       => '',
			  	      'container_class' => '',
			  	      'container_id'    => '',
			  	      'menu_class'      => 'navigation',
			  	      'menu_id'         => '',
			  	      'echo'            => true,
			  	      'fallback_cb'     => 'wp_page_menu',
			  	      'before'          => '',
			  	      'after'           => '',
			  	      'link_before'     => '',
			  	      'link_after'      => '',
			  	      'items_wrap' => '%3$s',
			  	      'depth'           => 0,
			  	      'walker'        => new themeslug_walker_nav_menu_header
			  	    );
			  	 		wp_nav_menu($default);
			    	?>
					</div>
		</div>



		<div class="footer__content">
			<?= $footer_content; ?>
		</div>
	</div>

</div>

<!-- <div class="sticky-element">
	<div class="sticky-element__close"><img src="<?php //bloginfo('template_url'); ?>/assets/img/close.png" alt=""></div>
		<img src="<?php //bloginfo('template_url'); ?>/assets/img/sticky-bar.png" alt="">
		<div class="sticky-element__wrapp">
			<div class="sticky-element__content">
				<h1>Special Event</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas faucibus mollis interdum. Curabitur blandit mpus porttitor. Curabitur blandit tempus.</p>
				<a href="#" class="full secondary button">Attend the Event &rarr;</a>
			</div>
		</div>
</div> -->

<?php if( (strpos( $_SERVER['REQUEST_URI'], 'church-registration') === false) && (strpos( $_SERVER['REQUEST_URI'], 'download-resources' ) === false) && (strpos( $_SERVER['REQUEST_URI'], 'churchkit' ) === false)): ?>
<div class="sticky-element__event text-center">
	<div class="sticky-element__event__wrapp">
		<div class="row">
			<div class="large-11 large-centered columns">
				<div class="sticky-element__event__content">
					<h2>Pastors & Churches - Equip your church with the videos<br class="hide-for-small-only"> and resources you need to make a difference in America!</h2>
					<?php if(isset($_COOKIE['resources'])): ?>
						<a href="<?php echo site_url(); ?>/download-resources/" class="white primary button stream-now">FREE Resources →</a>
					<?php else: ?>
						<a href="<?php echo site_url(); ?>/church-registration/" class="white primary button stream-now">FREE Resources →</a>
					<?php endif; ?>
				</div>
			</div>
		</div>

	</div>
</div>
<?php endif; ?>


<?php wp_footer(); ?>

<script type="text/javascript">
    (function($){
        $(document).ready(function(){
            var is_registered = "<?php echo $_COOKIE['resources']; ?>";
            var uri_current = "<?php echo $_SERVER['REQUEST_URI']; ?>";

            if((( uri_current == '/download-resources/' ) || ( uri_current == '/download-resources' )) && ( is_registered === '')){
            	$('body').hide();
            	window.location.href = "<?php echo site_url(); ?>/church-registration/";
            }
        });
    })(jQuery);
</script>

</body>
</html>
