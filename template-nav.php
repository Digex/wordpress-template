<?php
/*
*Template Name: Page Nav
*/
$sidebarpage_title = get_field('sidebarpage_title');
$sidebarpage_content = get_field('sidebarpage_content');

get_header();
get_sidebar('bannerpage'); 
?>
<div class="internal-page template-sidebar">
	<div class="row">
		<div class="large-9 large-push-3 columns">
			<div class="internal-page__content">
				<?php while(have_posts()) : the_post(); ?>
					<h1><?php the_title(); ?></h1>
				 <?php the_content();
				endwhile; ?>
			</div>
		</div>
		<div class="large-3 large-pull-9 columns">
			<div class="archive__sidebar">
				<ul>

					<?php
						$currentID = get_the_ID();
						$args = array(
							'post_type'  => 'page',
							'meta_key'  => '_wp_page_template',
							'meta_value' => 'template-nav.php',
							'posts_per_page' => -1,
							'order'	=> 'ASC',
							'orderby' => 'menu_order'				
						);
						$query1 = new WP_Query($args);
					?>
					<?php if ( $query1->have_posts() ) : while ( $query1->have_posts() ) : $query1->the_post();
						$curren = ($currentID == get_the_ID()) ? 'current-cat' : '' ;
					?>

						<li class="<?= $curren ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						
					<?php endwhile; wp_reset_postdata(); endif; ?>
				</ul>
			</div>	
		</div>
	</div>
</div>

<?php get_sidebar('socialmedia'); ?>

<?php
get_footer();
?>