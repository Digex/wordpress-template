<?php
/*
*Template Name: Page Pledge
*/
$sidebarpage_title = get_field('sidebarpage_title');
$sidebarpage_content = get_field('sidebarpage_content');
$pledge_image = get_field('pledge_image');

get_header();
get_sidebar('bannerpage'); 
?>
<div class="pledge-page-bg">
	<?php if($pledge_image) : ?>
		<img src="<?= $pledge_image['url']; ?>" alt="" style="min-height: <?= $pledge_image['height']; ?>px; min-width: <?= $pledge_image['width']; ?>px">
	<?php endif; ?>
	<div class="row">
		<div class="small-12 columns end">
			<div class="internal-page pledge-page">
				<div class="row">
					<?php while(have_posts()) : the_post(); ?>
						<?php the_content();
					endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_sidebar('socialmedia'); ?>

<?php
get_footer();
?>