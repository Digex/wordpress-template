(function($){
	$(document).ready(function($) {
		tinymce.PluginManager.add('button_custom', function( editor, url ) {
       editor.addButton( 'button_custom', {
           title: 'Insert Video',
           icon: 'icon page-break',
           image: url + '/../img/icon-youtube.png',
           onclick: function() {
               editor.insertContent('[youTube url=""]');
           }
       });
   });
	});

})(jQuery);