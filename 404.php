<?php get_header(); ?>

<div class="bannerpage">
	<div class="row">
		<div class="small-12 columns">
			<h1>Error 404</h1>
		</div>
	</div>
</div>

<div class="internal-page">
	<div class="row">
		<div class="small-12 columns">
			<div class="internal-page__content full-template">
				<h1>Page Not  Found</h1>

				<p>We are sorry, but whatever you were looking for is not here. </p>
				<p>Please visit our <a href="<?php bloginfo('url'); ?>" target="_self">homepage</a> or use the search box in the bottom section of this web page to try to locate it.</p>
			</div>
		</div>
		
	</div>
</div>

<?php get_sidebar('socialmedia'); ?>
<?php get_footer(); ?>