<?php
/*
*Template Name: Page Sidebar
*/
$sidebarpage_title = get_field('sidebarpage_title');
$sidebarpage_content = get_field('sidebarpage_content');

get_header();
get_sidebar('bannerpage'); 
?>
<div class="internal-page template-sidebar">
	<div class="row">
		<div class="large-9 large-push-3 columns">
			<div class="internal-page__content">
				<?php while(have_posts()) : the_post(); 
				  the_content();
				endwhile; ?>
			</div>
		</div>
		<div class="large-3 large-pull-9 columns">
			<div class="sidebar__title">
				<h3><?= $sidebarpage_title; ?></h3>
			</div>
			<div class="sidebar__content">
				<?= $sidebarpage_content; ?>
			</div>
			<div class="sidebar__butons">
				<?php 
					$top_bar_buttons = get_field('sidebarpage_buttons_top_bar_buttons');

					if($top_bar_buttons): foreach($top_bar_buttons as $top_bar_button):
      		$buttondata = $top_bar_button['nav_button_link'];
      		$typebutton = ($top_bar_button['nav_button_type'] == "hollow") ? "primary" : "secondary" ;
      		?>
      			<a href="<?= $buttondata['url']; ?>" class="<?= $top_bar_button['nav_button_type']; ?> <?= $typebutton; ?> button small" target="<?= $buttondata['target']; ?>">
      				<?= $buttondata['title']; ?>
      			</a>
						
      	<?php	endforeach; endif; ?>
			</div>		
		</div>
	</div>
</div>

<?php get_sidebar('socialmedia'); ?>

<?php
get_footer();
?>