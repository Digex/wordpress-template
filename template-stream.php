<?php
/*
*Template Name: Stream
*/
$stream_url = get_field('stream_url');
?>

<div class="hide">
    <?php
    get_header();
    ?>
</div>

<div class="stream-video">
    <div class="row">
        <div class="large-12 columns">
            <?php while(have_posts()) : the_post(); ?>
                <?php the_content();
            endwhile; ?>
        </div>
    </div>
    <div class="row">
        <div class="xlarge-10 xlarge-centered xxlarge-12 xxlarge-centered columns">
            <div class="responsive-embed widescreen">
              <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/FAK_Z097GkI" frameborder="0" allowfullscreen></iframe> -->
              <div id="myDiv"></div>
            </div>
            <a href="<?php bloginfo('url'); ?>" class="white primary button float-right">Continue on to KeepFaithInAmerica.com</a>
        </div>
    </div>
    <script src="https://content.jwplatform.com/libraries/gg1S4djT.js"></script>
    
    <script>
    jwplayer("myDiv").setup({
             "file": "https://d322z1ywfpkd3g.cloudfront.net/live/c9/playlist.m3u8?token_out=public",
             "image": "http://example.com/myImage.png",
             "mute": "false"
           });
    </script>
</div>

<div class="hide">
    <?php
    get_footer();
    ?>
</div>
