jQuery(document).foundation();


(function($){
	$(document).ready(function(){
    $('.sticky-element__close').on('click',function(){
      $(".sticky-element").animate({
        bottom: "-700"
      }, 1000 );
    });

    /*$('body').on('scrollme.zf.trigger', function(){
     var eTop =  $('.news-section').offset().top;
      if(eTop - $(window).scrollTop() < 0){
        if(!$(".sticky-element").hasClass('active') ){
          $(".sticky-element").animate({
            bottom: "20"
          }, 1000 );
          $(".sticky-element").addClass('active');
        }
      }
    });*/

		$('body').on('click', '.custom-css li.button', function(){
			$('.custom-css input.gform_button').click();
		});

    $('body').on('click', '.Live-Streaming li.button', function(){
      $('.Live-Streaming input.gform_button').click();
    });

    /**********
    *HTML IN LABEL
    **********/
    /*$('body').on('load', '.wrap-boxpledge', function(){
      $(this).clone().appendTo( "#boxpledge" );
      $(this).remove();
    });*/
    $('.wrap-boxpledge').each(function(){
      $(this).clone().appendTo( "#boxpledge" );
      $(this).remove();
    });


    $('.html > label').each(function(){
      $(this).html($(this).text());
    });





    $('.news-section__slide').slick({
    	slidesToShow: 3,
  	  slidesToScroll: 1,
  	  autoplay: true,
  	  autoplaySpeed: 8000,
  	  prevArrow: "<span class='slick-prev '></span>",
  	  nextArrow: "<span class='slick-next '></span>",

  	  responsive: [
  	  {
  	    breakpoint: 960,
  	    settings: {
  	    	slidesToShow: 2,
  	      arrows: false
  	    },
  	   },
  	  {
  	    breakpoint: 767,
  	    settings: {
  	    	slidesToShow: 1,
  	      arrows: false
  	    },
  	   }
  	   ]
    });

  });
})(jQuery);
