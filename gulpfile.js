var gulp = require('gulp'),
    babel = require('gulp-babel'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat');

var glp = require('gulp-load-plugins')();

var browserpx = "http://localhost:8080";

var sassPaths = [
  'bower_components/normalize.scss/sass',
  'bower_components/foundation-sites/scss',
  /*'bower_components/motion-ui/src',*/
];

var jsPaths = [
  /* Libraries required by Foundation */
  //"bower_components/jquery/dist/jquery.js",
  //"bower_components/what-input/dist/what-input.js",

  /* Core Foundation files */
  "bower_components/foundation-sites/dist/js/plugins/foundation.core.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.util.*.js",

  /* Individual Foundation components */
  /* If you aren't using a component, just remove it from the list */
  "bower_components/foundation-sites/dist/js/plugins/foundation.abide.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.accordion.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.accordionMenu.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.drilldown.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.dropdown.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.dropdownMenu.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.equalizer.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.interchange.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.magellan.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.offcanvas.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.orbit.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.responsiveMenu.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.responsiveToggle.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.reveal.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.slider.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.sticky.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.tabs.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.toggler.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.tooltip.js",
  "bower_components/foundation-sites/dist/js/plugins/foundation.zf.responsiveAccordionTabs.js",

  /* slick */
  "bower_components/slick-carousel/slick/slick.js",

  /* Paths to your own project code are here */
  "src/js/!(app).js",
  "src/js/app.js",
];

gulp.task('sass', function() {
  return gulp.src('src/scss/app.scss')
    .pipe(glp.sass({
      includePaths: sassPaths,
      outputStyle: 'compact' // if css compressed **file size**
    })
    .on('error', glp.sass.logError))
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.stream());
});

gulp.task('js', function() {
  return gulp.src(jsPaths)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(concat('app.min.js'))
    .pipe(uglify().on('error', function(e){
      console.log(e);
    }))
    .pipe(gulp.dest('assets/js'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('serve', ['sass', 'js'], function() {
  browserSync.init({
      proxy: browserpx,
      open: false
  });
  gulp.watch("./src/js/**/*.js", ['js']);
  gulp.watch("./src/scss/**/*.scss", ['sass']);
  gulp.watch("./**/*.php").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);
