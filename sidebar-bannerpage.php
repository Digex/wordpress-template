<?php
$banner_title = get_field('banner_title');
$banner_background = get_field('banner_background');
?>

<div class="bannerpage" style="background-image: url(<?= $banner_background['url']; ?>);">
	<div class="row">
		<div class="small-12 columns">
			<?php $banner_title = ($banner_title) ? $banner_title : get_the_title() ;?>
			<h1><?= $banner_title; ?></h1>
		</div>
	</div>
</div>