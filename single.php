<?php get_header();

$the_query = new WP_Query( 
		array(
			'category_name' => get_the_category()[0]->slug, 
			'posts_per_page' => 4,
			'post__not_in' => array( get_the_ID() )
		) 
	);
?>

<div class="bannerpage" style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/single.jpg);">
	<div class="row">
		<div class="small-12 columns">
			<h1>
					<?php 
				   $cat = get_the_category();
				   echo $cat[0]->name;
				  ?>
			</h1>
		</div>
	</div>
</div>
<div class="single-main">
	<div class="row">
		<div class="medium-8 large-7 columns print-section">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<b class="single-main__boxdate"><?php echo get_the_date(); ?></b>
				<div class="archive__boxsocial">
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="hollow primary button small small--grey" target="_blank"><img src="<?php bloginfo("template_url"); ?>/assets/img/button-face.png" alt=""> <span>Share</span></a> &nbsp;
					<a href="https://twitter.com/home?status=<?php echo get_the_title(); ?>%0A<?php echo get_permalink(); ?>" class="hollow primary button small small--grey" target="_blank"><img src="<?php bloginfo("template_url"); ?>/assets/img/button-twit.png" alt=""> <span>Tweet</span></a> &nbsp;
					<a href="mailto:?&amp;subject=<?php echo get_the_title(); ?>&amp;body=<?php echo get_permalink(); ?>"
					 class="hollow primary button small small--grey"><img src="<?php bloginfo('template_url'); ?>/assets/img/single-email.png" alt="" > <span>Mail</span></a> &nbsp;
					<a href="javascript:window.print()" class="hollow primary button small small--grey"><img src="<?php bloginfo('template_url'); ?>/assets/img/single-print.png" alt=""> <span>Print</span></a>
				</div>
				<?php if($cat[0]->slug == "videos"):
					$video_id = get_field('video_id'); ?>
					<div class="responsive-embed widescreen">
					  <iframe width="560" height="315" src="http://www.youtube.com/embed/<?= $video_id; ?>" frameborder="0" allowfullscreen></iframe>
					</div>
				<?php endif; ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</div>
		<div class="medium-4 large-4 columns print-section-hide">
			<div class="single-main__sidebar">
				<h1>More <?= $cat[0]->name; ?>
				</h1>
				<div class="single-main__box">
					<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					 	<div class="single-main__boxelement">
					 		<div class="small-4 columns no-padding-left archive__boximage">
					 			<?php 
					 				if($cat[0]->slug == "videos"):
					 				$video_id = get_field('video_id');
					 				?>
					 				<div class="responsive-embed widescreen">
					 				  <iframe width="560" height="315" src="http://www.youtube.com/embed/<?= $video_id; ?>" frameborder="0" allowfullscreen></iframe>
					 				</div>
					 			<?php else: ?>
					 				<a href="<?php the_permalink(); ?>">
					 					<?php the_post_thumbnail( 'thumbnail' ); ?>
					 				</a>
					 			<?php endif; ?>
					 		</div>
					 		<div class="small-8 columns no-padding">
					 			<div class="single-main__boxtitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
					 			<b class=""><?php echo get_the_date(); ?></b>
					 			<div class="single-main__boxcontent"><?= get_the_excerpt(); ?></div>
					 		</div>
					 	</div>
					<?php endwhile; wp_reset_postdata(); endif; ?>
				</div>
			</div>
		</div>

	</div>
</div>

<?php get_sidebar('socialmedia'); ?>

<?php get_footer(); ?>