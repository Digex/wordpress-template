<?php
/*
*Template Name: Page Contact
*/
$sidebarpage_title = get_field('sidebarpage_title');
$contact_image = get_field('contact_image');

get_header();
get_sidebar('bannerpage'); 
?>
<div class="contact-page-bg">
	<?php if($contact_image) : ?>
		<img src="<?= $contact_image['url']; ?>" alt="" style="min-height: <?= $contact_image['height']; ?>px; min-width: <?= $contact_image['width']; ?>px">
	<?php endif; ?>
	<div class="row">
		<div class="small-12 columns end">
			<div class="internal-page contact-page">
				<div class="row">
					<?php while(have_posts()) : the_post(); ?>
						<?php the_content();
					endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_sidebar('socialmedia'); ?>

<?php get_footer(); ?>