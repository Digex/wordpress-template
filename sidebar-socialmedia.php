<div class="social-section">
	<div class="row">
		<div class="medium-4 columns">
			<div class="social-section__wrap">
				<p class="social-section__title">Instagram</p>
				<!-- 6425352872 -->
				<?php echo do_shortcode('[fts_instagram instagram_id=6425352872 pics_count=4 type=user profile_wrap=no super_gallery=yes columns=2 force_columns=yes space_between_photos=3px icon_size=80px hide_date_likes_comments=yes]'); ?>
			</div>
		</div>
		<div class="medium-4 columns">
			<div class="social-section__wrap">
				<p class="social-section__title">Twitter</p>
				<?php echo do_shortcode('[fts_twitter twitter_name=@IStandForFaith twitter_height=auto tweets_count=3 popup=no hide_like_option=yes]'); ?>
			</div>
		</div>
		<div class="medium-4 columns">
			<div class="social-section__wrap">
				<p class="social-section__title">Facebook</p>
				<?php echo do_shortcode('[fts_facebook id=KeepFaithInAmerica posts_displayed=page_only posts=1 title=no description=no words=35 type=page hide_like_option=yes]'); ?>
			</div>
		</div>
	</div>
</div>
