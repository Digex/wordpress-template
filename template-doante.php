<?php
/*
*Template Name: Donate template
*/
$sidebarpage_title = get_field('sidebarpage_title');
$sidebarpage_content = get_field('sidebarpage_content');

get_header();
get_sidebar('bannerpage'); 
?>
<div class="internal-page donate-now">
	<div class="row">
		<div class="small-12 columns">
			<div class="internal-page__content full-template">
				<?php while(have_posts()) : the_post(); ?>
					<?php the_content();
				endwhile; ?>
			</div>
		</div>
		
	</div>
</div>


<?php get_footer(); ?>