<?php get_header();
$banner_title = get_field('banner_title', 'option');
$banner_background = get_field('banner_background', 'option');
?>

<div class="bannerpage" style="background-image: url(<?= $banner_background['url']; ?>);">
	<div class="row">
		<div class="small-12 columns">
			<?php $banner_title = ($banner_title) ? $banner_title : get_the_title() ;?>
			<h1><?= $banner_title; ?></h1>
		</div>
	</div>
</div>

<div class="page internal-page">
	<div class="row">
		<div class="large-9 large-push-3 columns">
			<div class="internal-page__content">
				<h1>
					<?php 
				    foreach((get_the_category()) as $category){
				      echo $category->name;
				    }
				  ?>
				</h1>
				<div>
				<?php while(have_posts()) : the_post(); ?>
					<div class="archive__box">
						<div class="medium-4 columns no-padding-left archive__boximage">
							<?php 
								if(is_category( 'videos' )):
								$video_id = get_field('video_id');
							?>
								<div class="responsive-embed widescreen">
								  <iframe width="560" height="315" src="http://www.youtube.com/embed/<?= $video_id; ?>" frameborder="0" allowfullscreen></iframe>
								</div>
							<?php else: ?>
								<?php the_post_thumbnail( 'large' ); ?>
							<?php endif; ?>
						</div>
						<div class="medium-8 columns">
							<div class="blognews__boxtitle archive__boxtitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
							<b class="archive__boxdate" ><?php echo get_the_date(); ?></b>
							<div class="blognews__boxcontent archive__boxcontent"><?= get_the_excerpt(); ?></div>
							<div class="archive__boxsocial">
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="hollow primary button small small--grey" target="_blank"><img src="<?php bloginfo("template_url"); ?>/assets/img/button-face.png" alt=""> <span>Share</span></a> &nbsp;
								<a href="https://twitter.com/home?status=<?php echo get_the_title(); ?>%0A<?php echo get_permalink(); ?>" class="hollow primary button small small--grey" target="_blank"><img src="<?php bloginfo("template_url"); ?>/assets/img/button-twit.png" alt=""> <span>Tweet</span></a>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				</div>
				<div class="archive__pagination">
					<div class="archive__button archive__button--left">
						<?php previous_posts_link(); ?>
					</div>
					<div class="archive__button archive__button--right">
						<?php next_posts_link(); ?>
					</div>
				</div>
				
			</div>
		</div>
		<div class="large-3 large-pull-9 columns">
			<div class="archive__sidebar">
				<ul>
					<?php 
						$args = array(
							'title_li' => '', 
							'hide_empty' => 1
						);
					  wp_list_categories_custom($args);
					?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php get_sidebar('socialmedia'); ?>

<?php get_footer(); ?>