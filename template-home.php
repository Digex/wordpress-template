<?php
/*
*Template Name: Home
*/
get_header();
$banner = get_field('banner');
$news = get_field('news');
$slides = $news['news_slide'];
$resources = get_field('resources');
$itemsfield = get_field('items');

?>
<!-- BANNER -->
<div class="banner" style="background-image: url('<?= $banner['banner_background_image']['url'] ?>');">
	<div class="row">
		<div class="large-10 small-11 columns small-centered">
			<div class="banner__box">
				<div class="banner__title">
					<?= $banner['banner_title']; ?>
				</div>
				<div class="banner__subtitle">
					<?= $banner['banner_sub_title']; ?>
				</div>
				<div class="banner__content">
					<?= $banner['banner_content']; ?>
				</div>
				
				<?php $rows = $banner['banner_buttons_top_bar_buttons']; 
					if($rows): 
					?>
					<div class="banner_buttons">
						<?php foreach($rows as $row): 
							$buttondata = $row['nav_button_link'];
							$typebutton = ($row['nav_button_type'] == "hollow") ? "primary" : "secondary" ;
						?>
							<a href="<?= $buttondata['url']; ?>" class="<?= $row['nav_button_type']; ?> <?= $typebutton; ?> button " target="<?= $buttondata['target']; ?>"><?= $buttondata['title']; ?></a>
						<?php endforeach; ?>
					</div>
				<?php	endif; ?>
			</div>
		</div>
	</div>
</div>
<!--/ BANNER -->

<!-- NEWS SECTION -->
<div class="news-section" style="background-image : url(<?= $news['news_background_image']['url']; ?>);">
	<div class="row">
		<div class="small-12 columns">
			<div class="news-section__mainbox">
				<h4>What We Stand for:</h4>
				<div class="news-section__slide">
					<?php 
						if($slides): 
							foreach($slides as $slide):
							print_r($slide);
					?>

						<div class="news-section__box">
							<div class="news-section__title"><?= $slide['news_slide_title']; ?></div>
							<div class="news-section__content"><?= $slide['news_slide_content']; ?></div>
						</div>
					<?php	
							endforeach; 
						endif; 
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<?php echo do_shortcode('[gravityform id="1" title="true" description="false" ajax="true"]'); ?>
		</div>
	</div>
</div>
<!-- /NEWS SECTION -->

<!-- RESOURCES -->
<div class="resources">
	<div class="row wrap-relative">

		<div class="large-6 columns">
			<div class="resources__box text-center">
				<h1 class="resources__title">
					<?= $resources['resources_title']; ?>
				</h1>
				<div class="resources__content">
					<?= $resources['resources_content'] ?>
				</div>
				<?php $rows = $resources['resources_buttons_top_bar_buttons'];
					if($rows): 
					?>
					<div class="resources__buttons">
						<?php foreach($rows as $row): 
							$buttondata = $row['nav_button_link'];
							$typebutton = ($row['nav_button_type'] == "hollow") ? "primary" : "secondary" ;
						?>
							<a href="<?= $buttondata['url']; ?>" class="<?= $row['nav_button_type']; ?> <?= $typebutton; ?> button " target="<?= $buttondata['target']; ?>"><?= $buttondata['title']; ?></a>
						<?php endforeach; ?>
					</div>
				<?php	endif; ?>
			</div>
		</div>

		<div class="resources__images content-absolute-right h100">
			<div class="table h100">
				<div class="table-cell vAmiddle">
					<?php if($resources['resources_image']) :?>
						<img src="<?= $resources['resources_image']['url']; ?>" alt="">
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /RESOURCES -->

<!-- BLOGNEWS -->
<?php
$the_query_news = new WP_Query( array('category_name' => 'news', 'posts_per_page'=>3) );
$the_query_videos = new WP_Query( array('category_name' => 'videos', 'posts_per_page'=>3));
?>
<div class="blognews">
	<div class="blognews__bg"></div>
	<div class="blognews__main">
		<div class="row">
			<div class="large-6 columns mobile-color">
				<div class="blognews__news">
					<h1>Latest News</h1>
					<div class="blognews__content">
						<?php if ( $the_query_news->have_posts() ) : while ( $the_query_news->have_posts() ) : $the_query_news->the_post(); ?>
							<div class="blognews__boxnews">
								<div class="medium-3 large-6 columns no-padding-left">
									<?php the_post_thumbnail( 'thumbnail' ); ?>
								</div>
								<div class="medium-9 large-6 columns">
									<div class="blognews__boxtitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
									<div class="blognews__boxcontent"><?= get_the_excerpt(); ?></div>
								</div>
							</div>
						<?php endwhile; wp_reset_postdata(); endif; ?>
					</div>
					<a href="<?php bloginfo('url');?>/category/news/" class="button primary hollow">More News &rarr;</a>
				</div>
			</div>
			<div class="large-6 columns">
				<div class="blognews__videos">
					<h1>Videos and More</h1>
					<div class="blognews__content">
						<?php if ( $the_query_videos->have_posts() ) : while ( $the_query_videos->have_posts() ) : $the_query_videos->the_post(); 
							$video_id = get_field('video_id');
						?>
							<div class="blognews__boxvideos">
								<div class="medium-3 large-6 columns no-padding-left">
								  <div class="responsive-embed widescreen">
								    <iframe width="560" height="315" src="http://www.youtube.com/embed/<?= $video_id; ?>" frameborder="0" allowfullscreen></iframe>
								  </div>
								</div>
								<div class="medium-9 large-6 columns no-padding-right">
									<div class="blognews__boxtitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
									<div class="blognews__boxcontent"><?= get_the_excerpt(); ?></div>
								</div>
							</div>
						<?php endwhile; wp_reset_postdata(); endif; ?>
						</div>
					<a href="<?php bloginfo('url');?>/category/videos/" class="button primary hollow">See All Media &rarr;</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /BLOGNEWS -->

<!-- ITEMS -->
<div class="items">
	<div class="row">
		<div class="medium-8 columns medium-centered">
			<h1><?= $itemsfield['items_title']; ?></h1>
			<div class="items__content">
				<?= $itemsfield['items_content']; ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<div class="items__row">
				<?php $rows = $itemsfield['items_item'];
					if($rows): foreach($rows as $row):
						$linkitem = ($row['item_selectlink'] == true) ? $row['items_pdf'] : $row['items_link']['url'] ;
						$linktarget = ($row['item_selectlink'] == true) ? '_blank' : $row['items_link']['target'] ;
						?>
						<div class="items__box">
							<a href="<?= $linkitem; ?>" target="<?= $linktarget; ?>">
								<img src="<?= $row['items_item_image']['url']; ?>" alt="<?= $row['items_item_image']['alt']; ?>">
								<div class="items__title">
									<?= $row['items_item_title']; ?>
								</div>
							</a>
						</div>
				<?php endforeach; endif; ?>
				</div>
		</div>
	</div>
</div>
<!-- /ITEMS -->

<?php get_sidebar('socialmedia'); ?>


<?php get_footer(); ?>