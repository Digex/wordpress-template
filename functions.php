<?php
/***************************
*VIDEO YOUTUBE
****************************/
function returnVYoutube($videoUrl){
  $pos = strpos($videoUrl, 'embed');
  if($pos){
    $recipe_regex = '/(\w{0,5}:\/\/www.\w+.com\/embed\/)([\w\-]+)/';
    preg_match_all($recipe_regex, $videoUrl, $matches);
    return $matches[2][0];
  }else{
    $recipe_regex = '/(\w+:\/\/www.\w+.com\/(\w+){0,1}\?{0,1}(\w\=)([\w\-\d]+))/';
    preg_match_all($recipe_regex, $videoUrl, $matches);
    return $matches[4][0];
  }
}
/*****************
*ADD CUSTOM BUTTON
******************/
add_action('init', 'shortcode_button_init');
function shortcode_button_init() {
  //Abort early if the user will never see TinyMCE
  if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
       return;

  //Add a callback to regiser our tinymce plugin
  add_filter("mce_external_plugins", "register_tinymce_plugin");

  // Add a callback to add our button to the TinyMCE toolbar
  add_filter('mce_buttons', 'add_tinymce_button');
}

//This callback registers our plug-in
function register_tinymce_plugin($plugin_array) {
    $plugin_array['button_custom'] = get_bloginfo('template_url') . '/assets/js/mce.js';
    return $plugin_array;
}

//This callback adds our button to the toolbar
function add_tinymce_button($buttons) {
            //Add the button ID to the $button array
    $buttons[] = "button_custom";
    return $buttons;
}

function buttonYoutube( $atts ) {
    extract( shortcode_atts(
     array(
      'url' => '',
     ), $atts ));

    ob_start(); ?>
      <div class="responsive-embed widescreen">
        <iframe height="315" frameborder="0" width="560" allowfullscreen="" src="http://www.youtube.com/embed/<?php echo returnVYoutube($url); ?>?rel=0&amp;showinfo=0"></iframe>
      </div>
    <?php return ob_get_clean();
}
add_shortcode( 'youTube', 'buttonYoutube' );

add_theme_support( 'post-thumbnails' );

/************************
*EXTENDS NAV
*************************/
register_nav_menus( array(
  'header_main_menu' => 'Header Menu',
  'header_top_menu' => 'Header Top Menu',
  'footer_menu' => 'Footer Menu'
));

/************************
*LOAD CSS AND Js
*************************/
function theme_styles(){
  wp_register_style( 'styles', get_template_directory_uri() . '/assets/css/app.min.css', array(), 'v.1.0.1', 'all' );
  wp_enqueue_style( 'styles' );
  wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'theme_styles');

function theme_scripts(){
  wp_enqueue_script('theme-scripts', get_template_directory_uri() . '/assets/js/app.min.js', array('jquery'), 'v.1', true);
}
add_action('wp_footer', 'theme_scripts');


/***************************
* SETTINGS PAGE
****************************/
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-settings'
  ));

  acf_add_options_sub_page(array(
      'title' => 'Header',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
  acf_add_options_sub_page(array(
      'title' => 'News & Events',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
  acf_add_options_sub_page(array(
      'title' => 'Footer',
      'parent' => 'theme-settings',
      'capability' => 'manage_options'
  ));
}

/***************************
*EXTENDS NAV
****************************/
$file = TEMPLATEPATH."/inc/extends-nav.php";
if(file_exists($file)){
    require_once($file);
}

function wp_list_categories_custom($arg) {
  $defaults = array (
    'taxonomy' => 'category',
    'hide_empty' => 1,
    'echo' => 1
  );
  $r = wp_parse_args($arg, $defaults);
  $list = "";
  $taxonomy = $r['taxonomy'];
  $hide_empty = $r['hide_empty'];
  $current_term_object = get_queried_object();
  // print_r($current_term_object);

  $args = array(
    'taxonomy' => $taxonomy,
    'hide_empty' => $hide_empty,
  );
  $categories = get_categories($args);
  foreach ($categories as $key => $category){
    $order_cat = get_field('order_cat', $taxonomy.'_' . $category->term_id);
    $termIDs[$order_cat] = $category->term_id;
  }

  ksort($termIDs);
  foreach($termIDs as $key => $termID){

    if(is_single()){
      $single = get_the_terms($current_term_object->ID, $taxonomy);
      $current_cat = ($single[0]->term_id == $termID) ? "current-cat" : '' ;
    }else{
      $current_cat = ($current_term_object->term_id == $termID) ? "current-cat" : '' ;
    }



    $term = get_term( $termID, $taxonomy );
    $list.= '<li class="cat-item cat-item-'.$termID.' '.$current_cat.'"><a href="'.get_term_link( $term ).'">'.$term->name.'</a></li>';
  }


  if ( $r['echo'] ) {
    echo  $list;
  }else{
    return $list;
  }

}

function nation_builder_construct($mail, $data_string){
  $json_url = 'https://cpcf.nationbuilder.com/api/v1/people/match?email='.$mail.'&access_token=356855310290107544d756db87f3f10e295874f6e77d54ce47a02a89e8100057';

    $match = curl_init( $json_url );

    $options = array(
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
    );

    curl_setopt_array( $match, $options );

    $result_match = curl_exec($match);
    $edit_user = '';
    $person = json_decode($result_match,true);
    if($person):
      foreach ($person as $people):
        if(is_array($people) && array_key_exists('id',$people)):
          $edit_user = $people['id'];
        endif;
      endforeach;
    endif;
    if($edit_user != ''):
      $add_user = $edit_user;
    endif;
    $add_user = ($edit_user) ? $edit_user : 'add' ;
    $put_url = 'https://cpcf.nationbuilder.com/api/v1/people/'.$add_user.'?access_token=356855310290107544d756db87f3f10e295874f6e77d54ce47a02a89e8100057';
    $ch = curl_init($put_url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Content-Length: ' . strlen($data_string))
    );
    $result = curl_exec($ch);
}
/************
*JOIN FORM
*************/
add_action( 'gform_after_submission_1', 'after_submission_joinnow', 10, 2 );
function after_submission_joinnow( $entry, $form ) {
  $first_name = (rgar($entry,'1')) ? rgar($entry,'1') : '';
  $last_name = (rgar($entry,'2')) ?  rgar($entry,'2') : '';
  $mail = (rgar($entry,'3')) ?  rgar($entry,'3') : '';
  $zip = (rgar($entry,'4')) ?  rgar($entry,'4') : '';
  $tags = 'N-KFA-Participant, N-Comm-Executive Director Email, N-Comm-Weekly Prayer Update, N-Comm-Action Alerts';
  $owner = '146027';

  $data = array(
    "person" => array(
      "email"=> $mail,
      "last_name"=> $last_name,
      "first_name"=> $first_name,
      'parent_id' => $owner,
      'tags' => array($tags),
      "registered_address" => array(
        "zip" => $zip
        )
      )
    );
  $data_string = json_encode($data);

  nation_builder_construct($mail, $data_string);
}

/************
*PLEDGE FORM
*************/
add_action( 'gform_after_submission_2', 'after_submission_pledge', 10, 2 );
function after_submission_pledge( $entry, $form ) {
  $first_name = (rgar($entry,'5')) ? rgar($entry,'5') : '';
  $last_name = (rgar($entry,'6')) ?  rgar($entry,'6') : '';
  $mail = (rgar($entry,'8')) ?  rgar($entry,'8') : '';
  $zip = (rgar($entry,'9')) ?  rgar($entry,'9') : '';
  $tags = 'N-KFA-Participant, N-Comm-Executive Director Email, N-Comm-Weekly Prayer Update, N-Comm-Action Alerts, N-KFA-Online Signer';
  $owner = '146027';

    $gfl = ( $entry['1.1'] ) ? $entry['1.1'].", " : '' ;
    $gfl .= ( $entry["1.2"] ) ? $entry["1.2"].", " : '';
    $gfl .= ( $entry["1.3"] ) ? $entry["1.3"] : '' ;

    $sfg = ( $entry["2.1"] ) ? $entry["2.1"].", " : '';
    $sfg .= ( $entry["2.2"] ) ? $entry["2.2"].", " : '' ;
    $sfg .= ( $entry["2.3"] ) ? $entry["2.3"].", " : '' ;
    $sfg .= ( $entry["2.4"] ) ? $entry["2.4"] : '' ;


  $data = array(
    "person" => array(
      "email"=> $mail,
      "last_name"=> $last_name,
      "first_name"=> $first_name,
      'parent_id' => $owner,
      'tags' => array($tags),
      'i_will_stand_for_my_faith_in_god_at' => $sfg,
      'i_will_pray_for_government_leaders' => $gfl,
      "registered_address" => array(
        "zip" => $zip
        )
      )
    );
  $data_string = json_encode($data);

  nation_builder_construct($mail, $data_string);
}

/************
*CONTACT FORM
*************/
add_action( 'gform_after_submission_3', 'after_submission_contact', 10, 2 );
function after_submission_contact( $entry, $form ) {
  $title = (rgar($entry,'1')) ? rgar($entry,'1') : '';
  $first_name = (rgar($entry,'2')) ?  rgar($entry,'2') : '';
  $last_name = (rgar($entry,'3')) ?  rgar($entry,'3') : '';
  $organization = (rgar($entry,'4')) ?  rgar($entry,'4') : '';
  $mail = (rgar($entry,'6')) ?  rgar($entry,'6') : '';

  $street = ( $entry["7.1"] ) ? $entry["7.1"] : '' ;
  $city   = ( $entry["7.3"] ) ? $entry["7.3"] : '' ;
  $state  = ( $entry["7.4"] ) ? $entry["7.4"] : '' ;
  $zip    = ( $entry["7.5"] ) ? $entry["7.5"] : '' ;

  $cgo = (rgar($entry,'8')) ?  rgar($entry,'8') : '';
  $hcwhy = (rgar($entry,'9')) ?  rgar($entry,'9') : '';

  $tags = 'N-Webform-KFA Contact Us, N-Comm-Do Not Email';
  $owner = '146027';

  $data = array(
    "person" => array(
      'title' => "$title",
      'occupation' => "$title",
      'first_name' => "$first_name",
      'last_name' => "$last_name",
      'email' => "$mail",
      'employer' => "$organization",
      'are_you_a_church_government_leader_or_organization' => "$cgo",
      'how_can_we_help_you' => "$hcwhy",
      'tags' => array($tags),
      'parent_id' => $owner,
      'registered_address' => array(
        'address1'=> $street,
        'address2' => NULL,
        'address3' => NULL,
        'city' => $city,
        'state' => $state,
        'country_code' => 'US',
        'zip'=> $zip
        )
      )
    );
  $data_string = json_encode($data);

  nation_builder_construct($mail, $data_string);
}

/*************************
* STREAM FORM
*************************/
add_action( 'gform_after_submission_4', 'after_submission_stream', 10, 2 );
function after_submission_stream( $entry, $form ) {
    setcookie( 'stream', '1', time() + (86400 * 30), '/' );

    $first_name = (rgar($entry,'6.3')) ? rgar($entry,'6.3') : '';
    $last_name = (rgar($entry,'6.6')) ?  rgar($entry,'6.6') : '';
    $mail = (rgar($entry,'2')) ?  rgar($entry,'2') : '';
    $tags = 'N-KFA-Participant, N-KFA Website-Watch Live, N-Comm-Executive Director Email, N-Comm-Weekly Prayer Update, N-Comm-Action Alerts';
    $owner = '146027';

    $data = array(
      "person" => array(
        "email"=> $mail,
        "last_name"=> $last_name,
        "first_name"=> $first_name,
        'parent_id' => $owner,
        'tags' => array($tags)
        )
      );
    $data_string = json_encode($data);

    nation_builder_construct($mail, $data_string);
}

/*************************
* GATED RESOURCES FORM
*************************/
add_action( 'gform_after_submission_5', 'after_submission_resources', 10, 2 );
function after_submission_resources( $entry, $form ) {
    setcookie( 'resources', '1', time() + (86400 * 30), '/' );

    $first_name = (rgar($entry,'1.3')) ? rgar($entry,'1.3') : '';
    $last_name = (rgar($entry,'1.6')) ? rgar($entry,'1.6') : '';
    $mail = (rgar($entry,'2')) ?  rgar($entry,'2') : '';
    $phone = (rgar($entry,'3')) ?  rgar($entry,'3') : '';
    $church_name = (rgar($entry,'4')) ?  rgar($entry,'4') : '';
    $city = (rgar($entry,'5')) ?  rgar($entry,'5') : '';
    $state = (rgar($entry,'6')) ?  rgar($entry,'6') : '';
    $tags = 'N-KFA-Participant, N-KFA Website-Church Resources, N-Comm-Executive Director Email, N-Comm-Weekly Prayer Update, N-Comm-Action Alerts';
    $owner = '146027';

    $data = array(
      "person" => array(
        "first_name"=> $first_name,
        "last_name"=> $last_name,
        "email"=> $mail,
        "phone"=> $phone,
        "church_name"=> $church_name,
        'parent_id' => $owner,
        'tags' => array($tags),
        'registered_address' => array(
          'city' => $city,
          'state' => $state,
          'country_code' => 'US'
          )
        )
      );
    $data_string = json_encode($data);

    nation_builder_construct($mail, $data_string);
}